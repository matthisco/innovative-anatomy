import { combineReducers } from "redux";
import { iaApp } from "./videos";

const rootReducer = combineReducers({
	iaApp
});

export default rootReducer;
