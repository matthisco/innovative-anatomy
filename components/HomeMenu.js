import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform,
  TouchableWithoutFeedback
} from "react-native";
import SplashScreen from "react-native-splash-screen";
import StyledText from "../components/StyledText";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { StackActions, NavigationActions } from "react-navigation";
export default class HomeMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    SplashScreen.hide();
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback
          onPress={() => {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "Drawer"
                })
              ]
            });
            this.props.navigation.dispatch(resetAction);
          }}
        >
          <View style={styles.menuItem}>
            <View style={styles.menuContainer}>
              <Image
                source={require("../assets/images/video-episodes.png")}
                style={styles.icon}
              />
              <Text style={styles.menuText}>
                <Text style={{ fontFamily: "Lato-Bold" }}> VIDEO</Text>
                <Text style={{ fontFamily: "Lato-Light" }}> EPISODES</Text>
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "Drawer"
                })
              ]
            });

            const navigateAction = NavigationActions.navigate({
              routeName: "Tests"
            });

            this.props.navigation.dispatch(resetAction);
            this.props.navigation.dispatch(navigateAction);
          }}
        >
          <View style={styles.menuItem}>
            <View style={styles.menuContainer}>
              <Image
                source={require("../assets/images/test-yourself.png")}
                style={styles.icon}
              />
              <Text style={styles.menuText}>
                <Text style={{ fontFamily: "Lato-Bold" }}> TEST</Text>
                <Text style={{ fontFamily: "Lato-Light" }}> YOURSELF</Text>
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "Drawer"
                })
              ]
            });
            const navigateAction = NavigationActions.navigate({
              routeName: "MyResults"
            });
            this.props.navigation.dispatch(resetAction);
            this.props.navigation.dispatch(navigateAction);
          }}
        >
          <View style={styles.menuItem}>
            <View style={styles.menuContainer}>
              <Image
                source={require("../assets/images/my-results.png")}
                style={styles.icon}
              />
              <Text style={styles.menuText}>
                <Text style={{ fontFamily: "Lato-Bold" }}> MY</Text>
                <Text style={{ fontFamily: "Lato-Light" }}> RESULTS</Text>
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "Drawer"
                })
              ]
            });

            const navigateAction = NavigationActions.navigate({
              routeName: "BookmarkedVideos"
            });

            this.props.navigation.dispatch(resetAction);
            this.props.navigation.dispatch(navigateAction);
          }}
        >
          <View style={styles.menuItem}>
            <View style={styles.menuContainer}>
              <Image
                source={require("../assets/images/bookmarked-videos.png")}
                style={styles.icon}
              />
              <Text style={styles.menuText}>
                <Text style={{ fontFamily: "Lato-Bold" }}> BOOKMARKED</Text>
                <Text style={{ fontFamily: "Lato-Light" }}> VIDEOS</Text>
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: "Drawer"
                })
              ]
            });

            const navigateAction = NavigationActions.navigate({
              routeName: "About"
            });

            this.props.navigation.dispatch(resetAction);
            this.props.navigation.dispatch(navigateAction);
          }}
        >
          <View style={styles.menuItem}>
            <View style={styles.menuContainer}>
              <Image
                source={require("../assets/images/about.png")}
                style={styles.icon}
              />
              <Text style={styles.menuText}>
                <Text style={{ fontFamily: "Lato-Bold" }}> ABOUT</Text>
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    paddingTop: 20,
    backgroundColor: "transparent",
    fontFamily: "lato-regular"
  },
  icon: {
    resizeMode: "contain",
    width: 45,
    height: 45,
    opacity: 1,
    marginRight: 10
  },
  menuContainer: {
    width: 220,
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row"
  },
  menuItem: {
    padding: 20,
    alignItems: "center",
    width: "100%",
    opacity: 0.8,
    backgroundColor: "#003366",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  menuText: {
    fontSize: 22,
    color: "#fff",
    opacity: 1,
    fontFamily: "lato-regular",
    textTransform: "uppercase"
  }
});
