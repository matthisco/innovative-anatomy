import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    FlatList,
    StyleSheet,
    StatusBar,
    Platform
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import {
    StackActions,
    NavigationActions,
    DrawerNavigator
} from "react-navigation";
import { ListItem, SearchBar } from "react-native-elements";
import { filteredVideo, initSearch } from "../actions/videos";
import StyledText from "../components/StyledText";
class DrawerHeader extends Component {
    constructor(props) {
        super(props);
        this.state = { term: "", videos: [] };
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
        this.resetForm = this.resetForm.bind(this);
    }
    componentDidMount() {
        this.SearchFilterFunction("");
    }
    SearchFilterFunction(term) {
        const { filteredVideo } = this.props;
        filteredVideo(term);
    }

    onSubmitEdit() {
        const {
            navigation: { navigate },
            initSearch
        } = this.props;

        navigate("VideoEpisodes");
    }

    onPress(id) {
        const {
            navigation,
            initSearch,
            navigation: { dispatch }
        } = this.props;
        initSearch();

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "VideoPlayer",
                    params: { id }
                })
            ]
        });
        dispatch(resetAction);
    }
    resetForm() {
        const {
            dispatch,
            goBack,
            state: { routeName }
        } = this.props.navigation;
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "Home"
                })
            ]
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.navigate("Home");
    }
    render() {
        const {
            navigation,
            videos,
            search: { term },
            scene: {
                route: { routeName: title }
            }
        } = this.props;
        return (
            <View>
                <View style={styles.container}>
                    <View
                        style={{
                            alignSelf: "flex-start"
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                width: 32,
                                marginLeft: 10,
                                marginRight: 5
                            }}
                            onPress={this.resetForm}
                        >
                            <Icon name="chevron-left" size={20} color="#fff" />
                        </TouchableOpacity>
                        <View
                            style={{
                                marginTop: 40,
                                marginLeft: 10
                            }}
                        >
                            <StyledText text={title} />
                        </View>
                    </View>
                    <View>
                        <Image
                            source={require("../assets/images/ia-logo.png")}
                            style={{
                                width: 160,
                                height: 60,
                                marginRight: 5,
                                padding: 10
                            }}
                        />

                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.openDrawer();
                            }}
                            style={{
                                margin: 10,
                                alignSelf: "flex-end"
                            }}
                        >
                            <Icon name="bars" size={20} color="#fff" />
                        </TouchableOpacity>
                    </View>
                </View>
                {title != "About" &&
                title != "Bookmarked Videos" &&
                title != "My Results" &&
                title != "Test Yourself" ? (
                    <SearchBar
                        round
                        containerStyle={{
                            borderTopWidth: 0,
                            borderBottomWidth: 0,
                            backgroundColor: "transparent"
                        }}
                        inputStyle={{ backgroundColor: "white" }}
                        inputContainerStyle={{
                            borderRadius: 5,
                            backgroundColor: "white"
                        }}
                        icon={{
                            type: "fontAwesome",
                            color: "#86939e",
                            name: "fa-search"
                        }}
                        onSubmitEditing={this.onSubmitEdit}
                        searchIcon={{ size: 18 }}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        onClear={text => this.SearchFilterFunction("")}
                        placeholder="Type Here..."
                        value={term}
                    />
                ) : null}
                {title != "Video Episodes" &&
                title != "About" &&
                title != "My Results" &&
                title != "Bookmarked Videos" &&
                title != "Test Yourself" ? (
                    <View>
                        <FlatList
                            data={this.props.search.videos}
                            renderItem={({ item }) => (
                                <ListItem
                                    title={item.title}
                                    onPress={() => this.onPress(item.id)}
                                />
                            )}
                            contentContainerStyle={{ overflow: "hidden" }}
                            keyExtractor={item => item.id.toString()}
                            ItemSeparatorComponent={this.renderSeparator}
                        />
                    </View>
                ) : null}
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    filteredVideo: data => dispatch(filteredVideo(data)),
    initSearch: () => dispatch(initSearch())
});

const mapStateToProps = state => {
    return {
        videos: state.iaApp.videos,
        search: state.iaApp.search
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DrawerHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingRight: 10,
        borderColor: "#ffffff50",
        borderBottomWidth: 1,
        marginTop: Platform.OS === "ios" ? 20 : 0,
        paddingTop: Platform.OS === "ios" ? 20 : 10
    },
    title: {
        color: "#fff",
        fontSize: 18
    }
});
