import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  Image,
  View,
  ActivityIndicator,
  TouchableHighlight
} from "react-native";
import { BackHandler } from "react-native";
import { connect } from "react-redux";

import { StackActions, NavigationActions } from "react-navigation";
import VideoList from "../components/VideoList";

import NavigationService from "../navigation/NavigationService.js";
class TestsScreen extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch } = this.props;

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Home" })]
    });
    this.props.navigation.dispatch(resetAction);
    return true;
  };

  render() {
    const {
      videos,
      search: { videos: searchedVideos }
    } = this.props;

    const {
      id,
      title,
      description,
      video,
      preview,
      push,
      dispatch,
      testLink
    } = this.props;

    let videoList = videos.filter(item => item.title.indexOf("QR") == -1);

    if (videos == "") {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      );
    }

    return (
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        {videoList.map((item, key) => {
          let id = item.id;

          return (
            <TouchableHighlight
              style={styles.episode}
              activeOpacity={1}
              key={key}
              underlayColor="#808080"
              onPress={() => {
                const resetAction = StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "Drawer",
                      params: { id }
                    })
                  ]
                });
                const navigateAction = NavigationActions.navigate({
                  routeName: "TestYourself",
                  params: { id }
                });
                this.props.navigation.dispatch(resetAction);
                this.props.navigation.dispatch(navigateAction);
              }}
            >
              <View style={styles.title}>
                <Text style={styles.text}>{item.title}</Text>
              </View>
            </TouchableHighlight>
          );
        })}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    videos: state.iaApp.videos,
    search: state.iaApp.search
  };
};

const mapDispatchToProps = dispatch => ({
  fetchVideos: () => dispatch(fetchVideos())
});

export default connect(mapStateToProps)(TestsScreen);

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    paddingBottom: 50,
    marginBottom: 50,
    justifyContent: "center",
    alignItems: "flex-start",
    paddingTop: 15,
    flexDirection: "row",
    flexWrap: "wrap"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  episode: {
    width: "100%",
    height: 40,
    margin: 1,
    opacity: 0.7,
    marginTop: 2,
    padding: 5,
    paddingLeft: 10,
    justifyContent: "flex-start", 
    backgroundColor: "#0D4C75"
  },
  icon: {},
  iconStyle: { width: 80, height: 80, paddingTop: 10 },
  title: {
    color: "white",
    flex: 1,
    flexWrap: "wrap"
  },
  content: {
    alignItems: "center"
  },
  text: {
    color: "white",
    fontSize: 16,
    justifyContent: "flex-start",
    marginTop: 5,
    fontFamily: "Lato-Regular"
  }
});
