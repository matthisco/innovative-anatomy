import React from "react";
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Picker,
  Dimensions,
  TouchableOpacity,
  Image
} from "react-native";
import Share from "react-native-share";
import ViewShot from "react-native-view-shot";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import FadeInView from "react-native-fade-in-view";
import StyledText from "../components/StyledText";
import TextWithFont from "../components/TextWithFont";
import ImageButton from "../components/ImageButton";
import { images } from "../helpers/images";
import Question from "../components/Question";
import NavigationService from "../navigation/NavigationService";
import {
  submitAnswer,
  resetQuiz,
  nextQuestion,
  completeQuiz
} from "../actions/videos";
class TestYourselfScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: () => null
  };

  constructor(props) {
    super(props);
    this.onCapture = this.onCapture.bind(this);
  }

  reset = id => {
    const { resetQuiz } = this.props;

    resetQuiz(id);
  };

  next = (id, current) => {
    const { nextQuestion } = this.props;

    nextQuestion({ id, current });
  };

  onCapture = uri => {
    const { id } = NavigationService.getParams();
    let video = this.props.videos.find(obj => obj.id == id);
    this.refs.viewShot.capture().then(uri => {
      const shareOptions = {
        title: "Share via",
        subject: `Check out my score for the ${video.title} module on the Tomorrow’s Clinicians app. Head over to www.tomorrowsclinicians.co.uk to try the app yourself.`,
        url: uri,
        message: `Check out my score for the ${video.title} module on the Tomorrow’s Clinicians app. Head over to www.tomorrowsclinicians.co.uk to try the app yourself.`
      };
      Share.open(shareOptions).catch(err => console.log(err));
    });
  };

  submitAnswer = (index, answer, id) => {
    let results = {};
    const { submitAnswer } = this.props;
    let video = this.props.videos.find(obj => obj.id == id);
    const questionsFiltered = video.questions.filter(obj => obj.question != "");
    let question = questionsFiltered[index];
    let questionsLength = questionsFiltered.length;
    let answers = question.answers.split("|");
    let completed = index === questionsLength - 1 ? true : false;
    let isCorrect = answers[question.correctAnswer].trim() == answer.trim();

    let correctAnswers = video.results.correctAnswers;
    if (isCorrect) {
      correctAnswers.push(index);
    }
    let incorrectAnswers = video.results.incorrectAnswers;
    if (!isCorrect) {
      incorrectAnswers.push(index);
    }

    let currentResult = {
      correctAnswers,
      incorrectAnswers
    };

    setTimeout(() => {
      submitAnswer({
        id,
        current: index,
        results: currentResult
      });
    }, 1000);
  };

  completeQuiz = id => {
    const { completeQuiz, videos, results } = this.props;
    let video = videos.find(obj => obj.id == id);
    let correctAnswers = video.results.correctAnswers;
    let incorrectAnswers = video.results.incorrectAnswers;
    const questionsFiltered = video.questions.filter(obj => obj.question != "");

    let currentResults = {
      correctAnswers,
      incorrectAnswers,
      totalScore: (correctAnswers.length / questionsFiltered.length) * 100
    };

    completeQuiz({
      id,
      currentResults
    });
  };

  render() {
    const screenWidth = Dimensions.get("window").width;
    const { id } = NavigationService.getParams();
    const { videos } = this.props;
    let video = videos.find(obj => obj.id == id);
    const {
      questions,
      completed,
      current,
      results,
      title,
      icon,
      total
    } = video;

    const questionsFiltered = questions.filter(obj => obj.question != "");

    let chartData = [];
    let imageSource = images[icon].uri;
    let lastQuestion =
      questionsFiltered.length ==
      results.incorrectAnswers.length + results.correctAnswers.length
        ? true
        : false;

    const background = completed === true ? "transparent" : "#ffffff40";

    chartData.push(results.totalScore);
    const chartConfig = {
      backgroundGradientFrom: "#1E2923",
      backgroundGradientTo: "#08130D",
      strokeWidth: 2
    };

    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            borderBottomWidth: 1,
            borderBottomColor: "#ffffff50"
          }}
        >
          <Image
            style={{
              justifyContent: "flex-start",
              width: 50,
              height: 40,
              margin: 10
            }}
            resizeMode="contain"
            source={imageSource}
          />

          <StyledText text={title} style={styles.title} />
        </View>

        {!completed === true && (
          <View style={{ alignItems: "flex-end", marginTop: 10 }}>
            <TextWithFont style={styles.questionCount}>
              Question {current + 1} out of {questionsFiltered.length}
            </TextWithFont>
          </View>
        )}

        <ScrollView
          contentContainerStyle={[
            styles.container,
            { backgroundColor: background }
          ]}
        >
          <View
            style={{
              backgroundColor: background,
              height: "100%",
              paddingBottom: 40
            }}
          >
            {questionsFiltered.length > 0 && !completed && (
              <View
                style={{
                  flex: 1,
                  flexDirection: "column"
                }}
              >
                <Question
                  onSelect={answer => {
                    this.submitAnswer(current, answer, id);
                  }}
                  title={title}
                  question={questionsFiltered[current]}
                  total={questionsFiltered.length}
                  results={results}
                  current={current}
                />
                {(results.correctAnswers.includes(current) ||
                  results.incorrectAnswers.includes(current)) &&
                  questionsFiltered[current].correctAnswerText != "" && (
                    <FadeInView duration={750} style={{ alignItems: "center" }}>
                      <TextWithFont style={styles.text}>
                        {questionsFiltered[current].correctAnswerText}
                      </TextWithFont>
                    </FadeInView>
                  )}
                <View style={{ flexDirection: "row" }}>
                  <Button
                    title={lastQuestion ? "FINISH" : "NEXT"}
                    buttonStyle={styles.button}
                    disabled={
                      !results.correctAnswers.includes(current) &&
                      !results.incorrectAnswers.includes(current)
                        ? true
                        : false
                    }
                    onPress={() =>
                      lastQuestion
                        ? this.completeQuiz(id)
                        : this.next(id, current)
                    }
                  />
                  <Button
                    title="RESTART QUIZ"
                    buttonStyle={styles.button}
                    onPress={() => this.reset(id)}
                  />
                </View>
              </View>
            )}
            {completed === true && (
              <View
                style={{
                  flex: 1
                }}
              >
                <ViewShot
                  ref="viewShot"
                  options={{ format: "jpg", quality: 0.9 }}
                  style={{ flex: 1 }}
                >
                  <View
                    style={{
                      backgroundColor: "#003366",
                      width: 400,
                      paddingBottom: 50,
                      paddingTop: 10
                    }}
                  >
                    <StyledText
                      text="TEST YOURSELF"
                      style={styles.textResult}
                      bold
                    />
                    <StyledText text={title} style={styles.textResult} bold />
                    <AnimatedCircularProgress
                      size={200}
                      style={styles.chart}
                      width={10}
                      fill={results.totalScore}
                      tintColor="#fff"
                      backgroundColor="#3d587580"
                    >
                      {fill => (
                        <>
                          <TextWithFont style={styles.scoreText}>
                            SCORE:
                          </TextWithFont>
                          <TextWithFont style={styles.score}>
                            {results.totalScore} %
                          </TextWithFont>
                        </>
                      )}
                    </AnimatedCircularProgress>
                    <View
                      style={{
                        position: "absolute",
                        left: 0,
                        bottom: 0,
                        width: 400,
                        height: 100,
                        padding: 5,
                        zIndex: 1,
                        flexDirection: "row",
                        justifyContent: "space-between"
                      }}
                    >
                      <View
                        style={{
                          alignItems: "flex-start"
                        }}
                      >
                        <Image
                          source={require("../assets/images/ia-logo.png")}
                          resizeMode="contain"
                          style={{
                            width: 90,
                            height: 50,
                            margin: 5,
                            marginLeft: 15
                          }}
                        />
                        <StyledText
                          text="tomorrowsclinicians.co.uk"
                          style={styles.textUrl}
                          bold
                        />
                      </View>
                      <View style={{ alignSelf: "flex-end" }}>
                        <Image
                          source={require("../assets/images/iClinicalLogo.png")}
                          resizeMode="contain"
                          style={{
                            alignSelf: "flex-start",
                            width: 100,
                            height: 50,
                            margin: 5,
                            marginRight: 10
                          }}
                        />
                      </View>
                    </View>
                  </View>
                </ViewShot>
                <View style={{ backgroundColor: "#006666", padding: 10 }}>
                  <ImageButton
                    text="SHARE YOUR SCORE"
                    image="share-icon"
                    onClick={this.onCapture}
                  />
                  <Button
                    title="RESTART QUIZ"
                    buttonStyle={styles.button}
                    onPress={() => this.reset(id)}
                  />
                </View>
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    videos: state.iaApp.videos
  };
};
const mapDispatchToProps = dispatch => ({
  submitAnswer: data => dispatch(submitAnswer(data)),
  resetQuiz: id => dispatch(resetQuiz(id)),
  nextQuestion: data => dispatch(nextQuestion(data)),
  completeQuiz: data => dispatch(completeQuiz(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestYourselfScreen);

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingBottom: 50,
    justifyContent: "center",
    alignItems: "flex-start",
    paddingTop: 15,
    flexDirection: "row",
    flexWrap: "wrap",
    backgroundColor: "#ffffff90"
  },
  titleText: { fontSize: 10 },
  chart: {
    alignSelf: "center"
  },
  questionCount: {
    backgroundColor: "white",
    padding: 3,
    color: "#135291",
    width: 140
  },
  loadingQuestions: {
    flex: 1
  },
  title: {
    fontSize: 16,
    justifyContent: "center",
    width: "80%"
  },
  score: {
    color: "white",
    fontSize: 50
  },
  scoreText: {
    color: "white",
    fontSize: 20
  },
  textUrl: { textTransform: "lowercase", marginLeft: 20 },
  textResult: {
    color: "white",
    justifyContent: "center",
    textAlign: "center",
    alignItems: "center",
    marginBottom: 10
  },
  text: {
    color: "#fff",
    fontSize: 16,
    padding: 5,
    lineHeight: 20,
    marginTop: 5,
    marginLeft: 20,
    borderRadius: 3,
    backgroundColor: "#006666"
  },
  button: {
    margin: 20,
    marginBottom: 40,
    flex: 6,
    padding: 10,
    backgroundColor: "#0CA8A8"
  }
});
