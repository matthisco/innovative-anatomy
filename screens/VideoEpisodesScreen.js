import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  Image,
  View,
  ActivityIndicator,
} from 'react-native';
import {BackHandler} from 'react-native';
import {connect} from 'react-redux';

import {StackActions, NavigationActions} from 'react-navigation';
import VideoList from '../components/VideoList';

import NavigationService from '../navigation/NavigationService.js';
class VideoEpisodesScreen extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    const {dispatch} = this.props;

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Home'})],
    });
    this.props.navigation.dispatch(resetAction);
    return true;
  };

  render() {
    const {
      videos,
      search: {videos: searchedVideos},
    } = this.props;

    let videoList =
      searchedVideos.length > 0
        ? searchedVideos
        : videos.filter(video => video.title.indexOf('QR') == -1);

    if (videos == '') {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      );
    }

    return (
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <VideoList videos={videoList} {...this.props.navigation} />
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    videos: state.iaApp.videos,
    search: state.iaApp.search,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchVideos: () => dispatch(fetchVideos()),
});

export default connect(mapStateToProps)(VideoEpisodesScreen);

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    paddingBottom: 50,
    marginBottom: 50,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
